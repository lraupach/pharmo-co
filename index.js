let arrayTsvData = [];

import myJsonData from "./pgxCheckResults.json" assert { type: "json" };
//import myJsonData from "./pgxCheckResults2.json" assert { type: "json" };

// Add patient data
document.getElementById("caseID").innerHTML = myJsonData.id;
document.getElementById("bmi").innerHTML = (parseFloat(myJsonData.demographics.weight) / Math.pow((parseFloat(myJsonData.demographics.height) / 100), 2)).toFixed(2);
let tooltip = new bootstrap.Tooltip(document.getElementById("bmi"), {
  title: "Weight: " + myJsonData.demographics.weight + myJsonData.demographics.weightUnit + "<br>Height: " + myJsonData.demographics.height + myJsonData.demographics.heightUnit,
  placement: 'right',
  html: true
});
document.getElementById("name").innerHTML =
  myJsonData.demographics.firstname + " " + myJsonData.demographics.lastname;
document.getElementById("age").innerHTML = myJsonData.demographics.age;
document.getElementById("gender").innerHTML = myJsonData.demographics.gender;
document.getElementById("pregnant").innerHTML =
  myJsonData.demographics.pregnant;
document.getElementById("disease").innerHTML =
  "(" + myJsonData.demographics.diseases.length + ")";
// 

const exampleEl = document.getElementById("disease");
tooltip = new bootstrap.Tooltip(exampleEl, {
  title: '<ul class="text-start">' +
    myJsonData.demographics.diseases
      .map((x) => "<li>" + x + "</li>")
      .join("") +
    "</ul>",
  placement: 'right',
  html: true
});

if (myJsonData.demographics.diseases.length > 0) {
  document.getElementById("disease").style.setProperty("background-color", "rgb(248, 215, 218)", "important");
  document.getElementById("disease").style.setProperty("color", "black", "important");
  let graphAlert = document.getElementById("legende-alert-container");

  const alert = document.createElement("div");
  alert.classList = "alert alert-danger d-flex align-items-center my-2";
  alert.role = "alert";

}

if (myJsonData.demographics.pregnancy == 'yes') {
  document.getElementById("pregnant").style.setProperty("background-color", "rgb(248, 215, 218)", "important");
  document.getElementById("pregnant").style.setProperty("color", "black", "important");
  let graphAlert = document.getElementById("legende-alert-container");

  const alert = document.createElement("div");
  alert.classList = "alert alert-danger d-flex align-items-center  my-2";
  alert.role = "alert";

  const divWithText = document.createElement("div");
  divWithText.innerHTML = "The person is pregnat "
  alert.appendChild(divWithText);
  graphAlert.appendChild(alert);

}


document.getElementById("contact").innerHTML =
  myJsonData.demographics.contact.number;

let geneVariants = myJsonData.demographics.geneVariants;
console.log(geneVariants)
let geneToGeneVariants = {}
for (const geneVariant of geneVariants) {
  const gene = geneVariant.split('*')[0];
  geneToGeneVariants[gene] = geneVariant;
}

let listOfAllDrugs = []
const selectObject = document.getElementById("selection");

for (let i = 0; i < myJsonData.drugs.length; i++) {
  const drug = myJsonData.drugs[i];
  listOfAllDrugs.push(drug.name);
  const optionObject = document.createElement("option");
  optionObject.innerHTML = drug.name;
  optionObject.value = drug.name;
  selectObject.appendChild(optionObject)

}


/**
 * add node name and index to dictionary if not exist and return index. Also if not exists add node to node list.
 * @param {*} name 
 * @returns 
 */
function startEndNodePreparation(name, dictNameToIndex, itemIndex, drugs, nodes, dose) {
  if (!(dictNameToIndex.hasOwnProperty(name))) {
    dictNameToIndex[name] = itemIndex;
    addNodeStartEnd(name, itemIndex, drugs, nodes, dose)
    itemIndex += 1;
  }

  return [dictNameToIndex[name], itemIndex];
}

/**
 * prepare node gene symbol and tooltip
 * @param {*} name 
 * @returns 
 */
function prepareGeneGuidline(name, indexOfDrugInJson) {
  let metabolArrow = "";
  let borderSymbolColor = "";
  let guidelineInfo = "";
  let guidelineId = "";
  let hasGuideline = false;
  // erstmal nur die erste Drug
  for (let k = 0; k < myJsonData.drugs[indexOfDrugInJson].guidelines.length; k++) {
    const guideline = myJsonData.drugs[indexOfDrugInJson].guidelines[k];
    if (
      name ==
      guideline.gene.name.replaceAll('"', "")
    ) {
      hasGuideline = true
      guidelineId = guideline.guidelineId;
      let arrowDosis;
      let dose;
      const arrowDown = "path://" +
        "M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zM8.5 4.5a.5.5 0 0 0-1 0v5.793L5.354 8.146a.5.5 0 1 0-.708.708l3 3a.5.5 0 0 0 .708 0l3-3a.5.5 0 0 0-.708-.708L8.5 10.293V4.5z"
      const arrowUpRight = "path://" +
        "M0 8a8 8 0 1 0 16 0A8 8 0 0 0 0 8zm5.904 2.803a.5.5 0 1 1-.707-.707L9.293 6H6.525a.5.5 0 1 1 0-1H10.5a.5.5 0 0 1 .5.5v3.975a.5.5 0 0 1-1 0V6.707l-4.096 4.096z";
      const arrowDownRight = "path://" +
        "M0 8a8 8 0 1 1 16 0A8 8 0 0 1 0 8zm5.904-2.803a.5.5 0 1 0-.707.707L9.293 10H6.525a.5.5 0 0 0 0 1H10.5a.5.5 0 0 0 .5-.5V6.525a.5.5 0 0 0-1 0v2.768L5.904 5.197z";
      const arrowUp = "path://" +
        "M46.875,258.333c0,116.003 95.456,211.459 211.458,211.459c116.003,-0 211.459,-95.456 211.459,-211.459c-0,-116.002 -95.456,-211.458 -211.459,-211.458c-116.002,-0 -211.458,95.456 -211.458,211.458Zm453.125,0c0,132.575 -109.092,241.667 -241.667,241.667c-132.574,0 -241.666,-109.092 -241.666,-241.667c-0,-132.574 109.092,-241.666 241.666,-241.666c132.575,-0 241.667,109.092 241.667,241.666Zm-226.563,105.73c0,8.285 -6.818,15.104 -15.104,15.104c-8.286,-0 -15.104,-6.819 -15.104,-15.104l0,-174.997l-64.827,64.857c-2.835,2.835 -6.684,4.429 -10.694,4.429c-8.296,0 -15.123,-6.826 -15.123,-15.123c0,-4.009 1.595,-7.859 4.43,-10.694l90.625,-90.625c2.832,-2.839 6.682,-4.437 10.693,-4.437c4.011,0 7.862,1.598 10.694,4.437l90.625,90.625c2.835,2.835 4.43,6.685 4.43,10.694c-0,8.297 -6.827,15.123 -15.124,15.123c-4.009,0 -7.858,-1.594 -10.693,-4.429l-64.828,-64.857l0,174.997Z";
      const arrowRight = "path://" +
        "M8 0a8 8 0 1 1 0 16A8 8 0 0 1 8 0zM4.5 7.5a.5.5 0 0 0 0 1h5.793l-2.147 2.146a.5.5 0 0 0 .708.708l3-3a.5.5 0 0 0 0-.708l-3-3a.5.5 0 1 0-.708.708L10.293 7.5H4.5z";
      const noArrow = "path://" +
        "M258.333,469.792c-116.002,-0 -211.458,-95.456 -211.458,-211.459c-0,-116.002 95.456,-211.458 211.458,-211.458c116.003,-0 211.459,95.456 211.459,211.458c-0,116.003 -95.456,211.459 -211.459,211.459Zm0,30.208c132.575,0 241.667,-109.092 241.667,-241.667c0,-132.574 -109.092,-241.666 -241.667,-241.666c-132.574,-0 -241.666,109.092 -241.666,241.666c-0,132.575 109.092,241.667 241.666,241.667Z";
      if (guideline.gene.metabolizerStatus == "Poor Metabolizer") {
        arrowDosis = "<strong>&#8599;</strong>";
        dose = "<span>&#9888;</span>";
        borderSymbolColor = "#06038D";
        metabolArrow = arrowDown;
      } else if (guideline.gene.metabolizerStatus == "Rapid Metabolizer") {
        arrowDosis = "<strong>&#8600;</strong>";
        borderSymbolColor = "#FFC000";
        metabolArrow = arrowUpRight;
      } else if (guideline.gene.metabolizerStatus == "Intermediate Metabolizer") {
        arrowDosis = "<strong>&#8599;</strong>";
        borderSymbolColor = "blue";
        metabolArrow = arrowDownRight;
      } else if (guideline.gene.metabolizerStatus == "Ultrarapid Metabolizer") {
        arrowDosis = "<strong>&#8599;</strong>";
        borderSymbolColor = "#FF0000";
        metabolArrow = arrowUp;
      } else if (guideline.gene.metabolizerStatus == "") {
        arrowDosis = "<strong>&#8594;</strong>";
        borderSymbolColor = "gray";
        metabolArrow = noArrow;
      } else {
        arrowDosis = "<strong>&#8594;</strong>";
        borderSymbolColor = "#32CD32";
        metabolArrow = arrowRight;
      }

      const severity = guideline.severity;
      const colorOfSeverity =
        severity == "high"
          ? "red"
          : severity == "medium"
            ? "orange"
            : "lime"; /* low */
      guidelineInfo = `<u><strong>Guideline ${guideline.guidelineId}</strong></u><br/>
      <strong>Dosage recommendation:</strong> ${guideline.recommendationDirections.join(", ")} <br/>
      <strong>Enzyme function:</strong> ${guideline.gene.metabolizerStatus}<br/>
      <strong>Activity score:</strong> ${guideline.gene.activityScore}<br/>
      <strong>Severity:</strong> <div style="display: inline-block; width: 14px; height: 14px; margin-right: 4px; border-radius: 7px; background-color: ${colorOfSeverity}"></div>${severity}`;
      
      break;
    } else {
      guidelineInfo = "Enzyme not present in the patient";
      borderSymbolColor = "gray";
    }
  }
  if (!(hasGuideline)) {
    if (name in geneToGeneVariants) {
      guidelineInfo = "The patient has the " + geneToGeneVariants[name] + " but no guidelines are known!";
      borderSymbolColor = "#dceb13";
    }
  }

  return [metabolArrow, borderSymbolColor, guidelineInfo, guidelineId]
}


/**
 *  Add start and end node to nodes with the fitting symbol
 * @param {} name 
 * @param {*} index 
 */
function addNodeStartEnd(name, index, drugs, nodes, dose) {
  let nodeValue;
  if (drugs.includes(name)) {
    nodeValue = {
      id: index,
      name: name,
      x: 100,
      y: 150,
      fixed: true,
      symbol:
        "path://M63.92,64.71a13.57,13.57,0,0,1,19-2.46h0a13.56,13.56,0,0,1,2.46,19C76,93.41,66.62,105.46,57.23,117.64h0a13.47,13.47,0,0,1-18.88,2.45h0a13.5,13.5,0,0,1-2.45-18.88h0c9.39-12.2,18.62-24.3,28-36.5ZM4.44,85.1H40.16l3.76-4.89c4.47-5.84,8.93-11.65,14.92-19.43v0a19.73,19.73,0,0,1,5.4-4.82V42.57c.1-8.13-4.08-17.13-8.48-23l-1.4-1.88h2.39a5,5,0,0,0,5-5V5a5.05,5.05,0,0,0-5-5H8.06A5.05,5.05,0,0,0,3,5v7.64a5,5,0,0,0,5,5H9.84c-.51.7-1,1.38-1.53,2.06C3.24,26.56-.51,35.34.06,44.28v55a7.91,7.91,0,0,0,2.11,5.49c1.36,1.32,3.34,2,6.06,1.92H26.86a19.45,19.45,0,0,1,.5-2.48,18.28,18.28,0,0,1,.6-1.86l-18.61,0a4.94,4.94,0,0,1-3.58-1.19,4.7,4.7,0,0,1-1.24-2.79c0-.22-.09-13.26-.09-13.26ZM29.29,50.32H35a1.94,1.94,0,0,1,1.94,1.94v6.52h6.52a1.94,1.94,0,0,1,1.94,1.93v5.7a1.94,1.94,0,0,1-1.94,1.94H36.92v6.52A1.94,1.94,0,0,1,35,76.8H29.29a1.94,1.94,0,0,1-1.94-1.93V68.35H20.83a1.94,1.94,0,0,1-1.93-1.94v-5.7a1.94,1.94,0,0,1,1.93-1.93h6.52V52.26a1.94,1.94,0,0,1,1.94-1.94ZM59.51,42H4.79c0-2.07-.13.09.09-1.84A43.67,43.67,0,0,1,12.55,21.3c.81-1.08,1.64-2.21,2.5-3.42H49.16c.76,1.06,1.58,2.15,2.39,3.24,3.69,4.9,7.2,13,7.85,19.11.07.6.11,1.79.11,1.79Zm7.29,58.22L50.55,87.72,38.79,103h0a10.28,10.28,0,0,0,1.87,14.38h0A10.29,10.29,0,0,0,55,115.5h0L66.8,100.24Z",

      itemStyle: {
        color: "purple",
      },
      symbolSize: 100,
      tooltip: {
        formatter: "Dose schema:" + dose, // Guideline Popups einfügen
      },
    };
  } else {
    nodeValue = {
      id: index,
      name: name,
      x: 28,
      y: 150,

      itemStyle: {
        //light violet
        color: "#DA70D6",
      },
      symbol: "rect",
      symbolSize: [50, 50],
    };
  }
  nodes.push(nodeValue);
}

async function prepareGudelineAndGraphForDrug(drugName) {

  let dose = ''

  // Add guidelines
  let guidelinesContainer = document.getElementById("guidelines");
  guidelinesContainer.replaceChildren();
  let indexOfDrugInJson = 0;
  for (let i = 0; i < myJsonData.drugs.length; i++) {
    const drug = myJsonData.drugs[i];
    if (drug.name !== drugName) {
      continue
    }
    indexOfDrugInJson = i;
    dose = drug.doseSchema;
    for (let j = 0; j < drug.guidelines.length; j++) {
      const guideline = drug.guidelines[j];
      const guidelineContainer = document.createElement("div");
      //guidelineContainer.classList = "d-grid gap-2 mb-1 d-block";
      guidelinesContainer.appendChild(guidelineContainer);

      guidelineContainer.innerHTML +=
        '<div class="d-grid gap-2 m-2"><button class="btn btn-secondary text-start collapseToggleIcon" type="button" data-bs-toggle="collapse" data-bs-target="#collapseGuideline' +
        guideline.guidelineId +
        '">' +
        drug.name + " guideline " + guideline.guidelineId +
        "</button></div>";
      const guidelineDetailsContainer = document.createElement("div");
      guidelineDetailsContainer.id = "collapseGuideline" + guideline.guidelineId;
      guidelineDetailsContainer.classList = "collapse text-bg-secondary m-2";
      guidelineDetailsContainer.innerHTML = `<table class="table mb-0" style="font-size: 14px">
          <tbody>
            <tr><th class="text-bg-secondary" scope="row">Gene:</th><td class="text-bg-secondary">${guideline.gene.name
        }</td></tr>
            <tr><th class="text-bg-secondary" scope="row">Diplotype:</th><td class="text-bg-secondary">${guideline.gene.diplotype.haplotype1
        }/ ${guideline.gene.diplotype.haplotype2}</td></tr>
            <tr><th class="text-bg-secondary" scope="row">Enzyme function:</th><td class="text-bg-secondary">${guideline.gene.metabolizerStatus
        }</td></tr>
            <tr><th class="text-bg-secondary" scope="row">Activity score:</th><td class="text-bg-secondary">${guideline.gene.activityScore
        }</td></tr>
          </tbody>
        </table>
        <div class="alert alert-light" role="alert" style="font-size: 12px">
          <b>Effect on metabolism:</b> <br>${guideline.effectOnMetabolism}
        </div>
        <div class="alert alert-warning" role="alert" style="font-size: 12px">
          <b>Recommendation:</b> <br>${guideline.recommendation}
        </div>
        <div class="d-grid gap-2">
          <a type="button" class="btn btn-sm btn-dark" target="_blank" rel="noopener noreferrer" href="${guideline.url}">Source <span class="iconify" data-icon="mdi-open-in-new"></span></a>
        </div>`;
      guidelineContainer.appendChild(guidelineDetailsContainer);

      new bootstrap.Collapse(guidelineDetailsContainer, { toggle: false });
    }

  }

  document.getElementById("drugs").innerHTML =
    "(" + listOfAllDrugs.length + ")";
  // 

  const drugsElemente = document.getElementById("drugs");
  tooltip = new bootstrap.Tooltip(drugsElemente, {
    title: '<ul class="text-start">' +
      listOfAllDrugs
        .map((x) => '<li>' + x + '</li>')
        .join("") +
      "</ul>",
    placement: 'right',
    html: true
  });


  // array of nodes
  let nodes = [];
  let links = [];

  let drugs = []

  // Read tsv-file
  //arrayTsvData = await d3.tsv("./Metoprolol.tsv");
  //drugs.push("metoprolol")
  if (drugName === 'metoprolol') {
    // whole metoprolol pathway
    arrayTsvData = await d3.tsv("./PA166179273.tsv");
    drugs.push("metoprolol")
  }
  // whole Amodiaquine pathway problem with split ", "only "," works
  //arrayTsvData = await d3.tsv("./PA165815256.tsv");
  //drugs.push("amodiaquine")
  // whole Acenocoumarol pathway
  // arrayTsvData = await d3.tsv("./PA166247041.tsv");
  // drugs.push("acenocoumarol")
  else if (drugName === 'atomoxetine') {
    // whole Atomoxetine pathway
    arrayTsvData = await d3.tsv("./PA166160830.tsv");
    drugs.push("atomoxetine")
  }
  // whole Amitriptyline and Nortriptyline pathway
  // arrayTsvData = await d3.tsv("./PA166163647.tsv");
  // drugs.push("nortriptyline")
  // drugs.push("amitriptyline")
  // whole Atorvastatin Pathway, Pharmacokinetics
  // arrayTsvData = await d3.tsv("./PA145011109.tsv");
  // drugs.push("atorvastatin")
  else if (drugName === 'clomipramine') {
    // whole Clomipramine Pathway I think best example because at least 2 guidlines really exists!
    arrayTsvData = await d3.tsv("./PA165960076.tsv");
    drugs.push("clomipramine")
  } else if (drugName === 'abacavir') {
    // whole Abacavir Pathway
    arrayTsvData = await d3.tsv("./PA166104634.tsv");
    drugs.push("abacavir")
  }else if (drugName === 'cannabidiol') {
    // whole Cannabidiol Pathway
    arrayTsvData = await d3.tsv("./PA166227081.tsv");
    drugs.push("cannabidiol")
  }else if (drugName === 'bupropion') {
    // whole Bupropion Pathway
    arrayTsvData = await d3.tsv("./PA166170276.tsv");
    drugs.push("bupropion")
  }


  // counter for new indices
  let itemIndex = 0;
  // dictionary form from/to name to index
  let dictNameToIndex = {};


  //Parse tsv data into a array of arrays [from, to, controler1, conroller2, ...](Stoffwechselweg)
  for (let x = 0; x < arrayTsvData.length; x++) {
    let from = arrayTsvData[x].From;
    let indexFromItemIndex = startEndNodePreparation(from, dictNameToIndex, itemIndex, drugs, nodes, dose);
    if (arrayTsvData[x]["Reaction Type"]!=='Biochemical Reaction')
      continue
    let indexFrom = indexFromItemIndex[0];
    itemIndex = indexFromItemIndex[1];

    let to = arrayTsvData[x].To;
    let indexToItemIndex = startEndNodePreparation(to, dictNameToIndex, itemIndex, drugs, nodes, dose);
    let indexTo = indexToItemIndex[0];
    itemIndex = indexToItemIndex[1];
    if (arrayTsvData[x].Controller.length > 0) {
      let arrayOfCyps = arrayTsvData[x].Controller.split(", ");
      for (const name of arrayOfCyps) {
        let nodeId = itemIndex;
        itemIndex += 1;
        const arrayMetaboliteArrowBorderSymbolColorGuidelineInfoguidelineId = prepareGeneGuidline(name, indexOfDrugInJson);
        const metabolArrow = arrayMetaboliteArrowBorderSymbolColorGuidelineInfoguidelineId[0];
        const borderSymbolColor = arrayMetaboliteArrowBorderSymbolColorGuidelineInfoguidelineId[1];
        const guidelineInfo = arrayMetaboliteArrowBorderSymbolColorGuidelineInfoguidelineId[2];
        const guidelineId = arrayMetaboliteArrowBorderSymbolColorGuidelineInfoguidelineId[3];
        const nodeValue = {
          id: nodeId,
          name: name,

          x: 131,
          y: 75,
          symbol: metabolArrow,
          gene: true,

          itemStyle: {
            //light blue
            color: "#89CFF0",
            borderWidth: 2,
            borderColor: borderSymbolColor,
          },
          tooltip: {
            formatter: guidelineInfo, // Guideline Popups einfügen
          },
        };
        if (guidelineId !== "") {
          nodeValue["guidelineId"] = guidelineId;
        }
        nodes.push(nodeValue);

        let linkValue = {
          source: indexFrom,
          target: nodeId,
        };
        links.push(linkValue);

        linkValue = {
          source: nodeId,
          target: indexTo,
        };
        links.push(linkValue);

      }
    } else {
      let linkValue = {
        source: indexFrom,
        target: indexTo,
      };
      links.push(linkValue);
    }

  }



  let chartContainer = document.getElementById("chart-container");
  let myChart = echarts.init(chartContainer, null, {
    renderer: "canvas",
    useDirtyRect: false,
  });
  myChart.setOption({
    tooltip: {
      outside: true,
    },
    legend: {
      layout: "vertical",
      align: "right",
      verticalAlign: "middle",
      borderWidth: 0,
    },
    animationDurationUpdate: 1500,
    animationEasingUpdate: "quinticInOut",
    series: [
      {
        type: "graph",
        layout: "force",
        force: {
          // initLayout:'circular',
          edgeLength: 150,
          gravity: 0.01,
          repulsion: 700,
        },
        symbolSize: 50,
        roam: true,
        label: {
          show: true,
          position: "bottom",
        },
        edgeSymbol: ["circle", "arrow"],
        edgeSymbolSize: [4, 10],
        edgeLabel: {
          fontSize: 25,
        },
        draggable: true,
        data: nodes,
        links: links,

        lineStyle: {
          opacity: 0.9,
          width: 2,
          curveness: 0,
        },
        emphasis: {
          focus: 'adjacency',
        }
      },
    ],
  });
  chartContainer.addEventListener("click", (e) => {
    e.target.innerHTML = "Show series";
  });

  window.addEventListener("resize", myChart.resize);

  myChart.on('click', function (params) {
    if (params.componentType === 'series') {
      if (params.seriesType === 'graph') {
        if (params.dataType === 'node') {
          if ('guidelineId' in params.data) {
            document.getElementById("collapseGuideline" + params.data.guidelineId).classList.add("show");
          }
        }
      }
    }
  });

  /*
  myChart.on('mouseover', function (params) {
    if (params.componentType === 'series') {
      if (params.seriesType === 'graph') {
        if (params.dataType === 'node') {
          if ('gene' in params.data) {
            let geneName= params.data.name;
            for (let node of nodes){
              if (node.name===geneName){
                console.log(node)
                node.itemStyle.opacity = 0.9;
              }
                
            }
  
          }
        }
      }
    }
  })*/

  const tooltipTriggerList = document.querySelectorAll('[data-bs-toggle="tooltip"]');
  [...tooltipTriggerList].map(tooltipTriggerEl => new bootstrap.Tooltip(tooltipTriggerEl));
}

prepareGudelineAndGraphForDrug(selectObject.value);
selectObject.addEventListener('input', () => prepareGudelineAndGraphForDrug(selectObject.value));

